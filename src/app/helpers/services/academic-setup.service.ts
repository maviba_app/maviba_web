import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { CONFIG } from '../../app.constant';
import { LoginUserService } from './login-user.service';

@Injectable()
export class AcademicSetupService {

  constructor(private _http: HttpClient, private http: Http, private auth: LoginUserService) {
      }
  getAllClasses(): Observable < any > {
    const headers = new Headers({ Authorization: this.auth.apiKey() });
    const options = new RequestOptions({
      headers: headers,
    });

    const URL = 'http://localhost:3000/classes';
    return this.http.get(URL, options)
    .map((response: Response) => {
      return response.json();
    });
  }
  createClasses(data): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey() });
    const options = new RequestOptions({ headers: headers });
    const obj = {
        id: data.id,
        className: data.className,
    };
    const URL = 'http://localhost:3000/classes/';
    // CONFIG.URL.BASE + CONFIG.URL.CREATE_SCHOOL
    return this.http.post(URL, obj, options)
        .map((response: Response) => {
            return response.json();
        });
  }

  updateClasses(data): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey()});
    const options = new RequestOptions({ headers: headers });
    const obj = {
      className: data.className,
  };
  const URL =  'http://localhost:3000/classes/' + data.id;
  //  const URL = CONFIG.URL.BASE + CONFIG.URL.UPDATE_SCHOOL + '/' + data.schoolID;
    return this.http.put(URL, obj, options)
        .map((response: Response) => {
          return response.json();
        });
  }

  getAllSubject(): Observable < any > {
    const headers = new Headers({ Authorization: this.auth.apiKey() });
    const options = new RequestOptions({
      headers: headers,
    });

    const URL = 'http://localhost:3000/subjects';
    return this.http.get(URL, options)
    .map((response: Response) => {
      return response.json();
    });
  }
  createSubject(data): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey() });
    const options = new RequestOptions({ headers: headers });
    const obj = {
        id: data.id,
        subjectName: data.subjectName,
    };
    const URL = 'http://localhost:3000/subjects/';
    // CONFIG.URL.BASE + CONFIG.URL.CREATE_SCHOOL
    return this.http.post(URL, obj, options)
        .map((response: Response) => {
            return response.json();
        });
  }

  updateSubject(data): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey()});
    const options = new RequestOptions({ headers: headers });
    const obj = {
      subjectName: data.subjectName,
  };
  const URL =  'http://localhost:3000/subjects/' + data.id;
  //  const URL = CONFIG.URL.BASE + CONFIG.URL.UPDATE_SCHOOL + '/' + data.schoolID;
    return this.http.put(URL, obj, options)
        .map((response: Response) => {
          return response.json();
        });
  }

}
