import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

import { iInstitute } from '../interfaces/iinstitute';
import { CONFIG } from '../../app.constant';
import { LoginUserService } from './login-user.service';

@Injectable()
export class InstituteService {

  constructor(private http: Http, private auth: LoginUserService) {
      }
  getInstitute(): Observable < iInstitute[] > {
    const headers = new Headers({ Authorization: this.auth.apiKey() });
    const options = new RequestOptions({
      headers: headers,
    });
    const URL = CONFIG.URL.BASE + CONFIG.URL.GET_ALL_INSTITUTE;
    console.log('Sending post request', URL);
    return this.http.get(URL, options)
    .map((response: Response) => {
      return response.json();
    });
  }

  createInstitute(data, instituteLogo): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey() });
    const options = new RequestOptions({ headers: headers });
    const obj = {
        instituteCode: data.instituteCode,
        instituteName: data.instituteName,
        instituteLogo: instituteLogo,
        userID: data.userID,
        adminName: data.userName,
        adminEmailID: data.email,
        createdRole: this.auth.userRole(),
        createdBy: this.auth.userId(),
        instituteAddress: {
          address: data.instituteAddress,
          city: data.instituteCity,
          district: data.instituteDistrict,
          state: data.instituteState,
          pincode: data.institutePinCode,
        },
    };
    const filteredObj = JSON.parse(JSON.stringify(obj));
    const URL = CONFIG.URL.BASE + CONFIG.URL.CREATE_INSTITUTE;
    console.log('Sending post request', URL, filteredObj);

    return this.http.post(URL, filteredObj, options)
        .map((response: Response) => {
            return response.json();
        });
  }

  updateInstitute(data, instituteLogo): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey() });
    const options = new RequestOptions({ headers: headers });
    const obj = {
      instituteName: data.instituteName,
      instituteLogo: instituteLogo,
      userID: data.userID,
      adminName: data.userName,
      adminEmailID: data.email,
      instituteAddress: {
        address: data.instituteAddress,
        city: data.instituteCity,
        district: data.instituteDistrict,
        state: data.instituteState,
        pincode: data.institutePinCode,
      },
    };

    const filteredObj = JSON.parse(JSON.stringify(obj));
    const URL = CONFIG.URL.BASE + CONFIG.URL.UPDATE_INSTITUTE + '/' + data.instituteID;
    console.log('Sending put request', URL, filteredObj);
    return this.http.put(CONFIG.URL.BASE + CONFIG.URL.UPDATE_INSTITUTE + '/' + data.instituteID, filteredObj, options)
        .map((response: Response) => {
          return response.json();
        });
  }


  getInstituteByUserId(instituteId?: string): Observable <iInstitute> {
    const id = instituteId ? instituteId : this.auth.userId();
    const headers = new Headers({ Authorization: this.auth.apiKey() });
    const options = new RequestOptions({
      headers: headers,
      params: 'userID=' + id,
    });
    const URL = CONFIG.URL.BASE + CONFIG.URL.GET_INSTITUTE_DETAIL;
    console.log('Sending get request', URL);
    return this.http.get(URL, options)
    .map((response: Response) => {
        return response.json();
    });
  }
}
