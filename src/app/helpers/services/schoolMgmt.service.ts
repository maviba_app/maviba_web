import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { LoginUserService } from './login-user.service';

import { CONFIG } from '../../app.constant';

@Injectable()
export class SchoolMgmtService {

  totalUser: number;
  constructor(private _http: HttpClient, private http: Http, private auth: LoginUserService) {}
  getSchool(): Observable <any> {
    const headers = new Headers({ Authorization: this.auth.apiKey() });
    const options = new RequestOptions({
      headers: headers,
      // params: 'userID=' + this.auth.userId(),
    });
    const URL = 'http://localhost:3000/getSchools';
    // CONFIG.URL.BASE + CONFIG.URL.GET_ALL_SCHOOL
    return this.http.get(URL, options)
    .map((response: Response) => {
      this.totalUser = response.json().length;
      return response.json();
    });
  }

  createSchool(data, schoolLogo): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey() });
    const options = new RequestOptions({ headers: headers });
    const obj = {
        id: this.totalUser + 1,
        schoolCode: data.schoolCode,
        schoolName: data.schoolName,
        schoolLogo: schoolLogo,
        userID: data.userID,
        createdRole: this.auth.userRole(),
        createdBy: this.auth.userId(),
        schoolAddress: {
          address: data.schoolAddress,
          city: data.schoolCity,
          district: data.schoolDistrict,
          state: data.schoolState,
          pincode: data.schoolPinCode,
        },
    };
    const URL = 'http://localhost:3000/getSchools/';
    // CONFIG.URL.BASE + CONFIG.URL.CREATE_SCHOOL
    return this.http.post(URL, obj, options)
        .map((response: Response) => {
            return response.json();
        });
  }

  updateSchool(data, schoolLogo): Observable <any> {
    const headers = new Headers({ 'Content-Type': 'application/json', Authorization: this.auth.apiKey()});
    const options = new RequestOptions({ headers: headers });
    const obj = {
      schoolCode: data.schoolCode,
      schoolName: data.schoolName,
      schoolLogo: schoolLogo,
      userID: data.userID,
      schoolAddress: {
        address: data.schoolAddress,
        city: data.schoolCity,
        district: data.schoolDistrict,
        state: data.schoolState,
        pincode: data.schoolPinCode,
      },
  };
  const URL =  'http://localhost:3000/getSchools/' + data.id;
  //  const URL = CONFIG.URL.BASE + CONFIG.URL.UPDATE_SCHOOL + '/' + data.schoolID;
    return this.http.put(URL, obj, options)
        .map((response: Response) => {
          return response.json();
        });
  }


  getSchoolByUserId(schoolId?: string): Observable <any> {
    const id = schoolId ? schoolId : this.auth.userId();
    const headers = new Headers({ Authorization: this.auth.apiKey() });
    const options = new RequestOptions({
      headers: headers,
    });

    return this.http.get(CONFIG.URL.BASE + CONFIG.URL.GET_INSTITUTE_DETAIL + '/' + id, options)
    .map((response: Response) => {
        return response.json();
    });
  }

  private handleError(err: HttpErrorResponse) {
    return Observable.throw(err.message);
  }

}
