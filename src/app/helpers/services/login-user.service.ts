import {Injectable} from '@angular/core';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { Router, ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';
import { CONFIG } from '../../app.constant';

@Injectable()
export class LoginUserService {

    constructor(private router: Router) {}

    apiKey(): string {
        const currentUser = JSON.parse(localStorage.getItem('loggedInUser'));
        return currentUser.apiKey;
    }

    userId(): string {
        const currentUser = JSON.parse(localStorage.getItem('loggedInUser'));
        const userDetails = currentUser && currentUser.userDetails ? currentUser.userDetails : '';
        const userId = userDetails && currentUser.userDetails.userID ? currentUser.userDetails.userID : '';
        return userId;
    }

    userRole(): string {
        const currentUser = JSON.parse(localStorage.getItem('loggedInUser'));
        const userDetails = currentUser && currentUser.userDetails ? currentUser.userDetails : '';
        const userRole = userDetails && currentUser.userDetails.userRole ? currentUser.userDetails.userRole : '';
        return userRole;
    }

    loggedIn() {
        return this.apiKey();
    }

    handleResponse(response: Response) {
        if (response.status === 401) {
          localStorage.removeItem('loggedInUser');
          this.router.navigate([CONFIG.ROUTES.LOGIN]);
        } else {
          return response;
        }
    }
}
