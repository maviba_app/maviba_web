import { Pipe, PipeTransform } from '@angular/core';
import { iInstitute } from '../interfaces/iinstitute';

@Pipe({
  name: 'instituteFilter'
})
export class InstituteFilterPipe implements PipeTransform {

  transform(value: iInstitute[], args?: string): iInstitute[] {
    args = (args) ? args.toLocaleLowerCase() : null;
    return (args) ? value.filter(
          (institute: iInstitute) => institute.instituteName.toLocaleLowerCase().indexOf(args) !==-1 
          ) : value;
  }

}
