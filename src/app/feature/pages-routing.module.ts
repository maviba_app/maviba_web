import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { InstituteListComponent } from './instituteMgmt/institute-list/institute-list.component';
import { CONFIG } from '../app.constant';
import { SchoolListComponent } from './schoolMgmt/school-list/school-list.component';
import { InstituteProfileComponent } from './instituteProfile/institute-profile.component';
import { AuthGuard } from '../helpers/services/auth-guard.service';
import { AcademicSetupComponent } from './academicSetup/academicSetup.component';

const routes: Routes = [{
  path: CONFIG.ROUTES.PRIVATE,
  component: PagesComponent,
  canActivate: [AuthGuard],
  children: [{
    path: CONFIG.ROUTES.DASHBOARD,
    component: DashboardComponent,
    canActivate: [AuthGuard],
  }, {
    path: CONFIG.ROUTES.INSTITUTE,
    component: InstituteListComponent,
    canActivate: [AuthGuard],
  }, {
    path: CONFIG.ROUTES.SCHOOL_MGMT,
    component: SchoolListComponent,
    canActivate: [AuthGuard],
  }, {
    path: CONFIG.ROUTES.INSTITUTE_PROFILE,
    component: InstituteProfileComponent,
    canActivate: [AuthGuard],
  }, {
    path: CONFIG.ROUTES.ACADEMIC_SETUP,
    component: AcademicSetupComponent,
    canActivate: [AuthGuard],
  }, {
    path: '',
    redirectTo: CONFIG.ROUTES.DASHBOARD,
    pathMatch: 'full',
  }],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [AuthGuard],
})
export class PagesRoutingModule {
}
