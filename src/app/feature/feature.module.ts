import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { CommonModule } from '@angular/common';
import { DashboardModule, InstituteMgmtModule, SchoolMgmtModule, AcademicSetupModule } from './index';
import { ThemeModule } from '../@theme/theme.module';
import { PagesRoutingModule } from './pages-routing.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { InstituteProfileComponent } from './instituteProfile/institute-profile.component';
import { HighlightDirective } from '../helpers/directives/highlight.directive';
import { OrderByPipe } from '../helpers/filters/filter';

const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    DashboardModule,
    InstituteMgmtModule,
    SchoolMgmtModule,
    PagesRoutingModule,
    Ng2SmartTableModule,
    AcademicSetupModule,
  ],
  declarations: [
    ...PAGES_COMPONENTS, InstituteProfileComponent, HighlightDirective, OrderByPipe,
  ],
})
export class PagesModule {
}
