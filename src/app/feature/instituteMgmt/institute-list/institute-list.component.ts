import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { iInstitute } from '../../../helpers/interfaces/iinstitute';
import { PagerService } from '../../../helpers/services/pagination.service';
import { InstituteService, AlertService, LoginUserService } from '../../../helpers/services/service';
import { CONFIG } from '../../../app.constant';
import * as _ from 'underscore';

@Component({
  selector: 'app-institute-list',
  templateUrl: './institute-list.component.html',
  styleUrls: ['./institute-list.component.scss'],
  // styles: ['.custom {color : blue}'],
  // providers: [InstituteService]
})
export class InstituteListComponent implements OnInit {
  _router: Router;
  listFilter= '';
  institutes: iInstitute[];
  instituteDetail: iInstitute;
  selectedInstituteDetail: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  modalReference: any;
  closeResult: string;
  url: string;
  model: any = {};
  loading = false;
  message: string;

    constructor(
        private router: Router,
        private modalService: NgbModal,
        private instituteService: InstituteService,
        private alertService: AlertService,
        private pagerService: PagerService,
        private auth: LoginUserService,
    ) { }

    ngOnInit() {
        this.getAllInstitute();
    }

    getAllInstitute() {
        this.instituteService.getInstitute()
        .subscribe(
            institutes => {
                if (institutes) {
                    this.institutes = institutes.sort(function(a, b) {
                        return (a.createdDate < b.createdDate) ? 1 : ((b.createdDate < a.createdDate) ? -1 : 0);
                    });
                    this.setPage(1);
                } else {
                    console.log("Data error");
                }
            },
            error => {
                this.auth.handleResponse(error);
            },
        );
    }

    openModelWithDetail(content, instituteDetail) {
        this.selectedInstituteDetail = instituteDetail;
        this.modalService.open(content, {size : 'lg'});
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        // get pager object from service
        this.pager = this.pagerService.getPager(this.institutes.length, page);
        // get current page of items
        this.pagedItems = this.institutes.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    open(content, selectedModel) {
        this.model = this.getModelObject(selectedModel);
        this.modalReference = this.modalService.open(content, {size : 'lg'});
        this.modalReference.result.then((result) => {
            this.message = '';
            this.model = {};
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.message = '';
            this.model = {};
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    getModelObject(selectedModel) {
        return {
            instituteID: selectedModel.instituteID,
            instituteCode : selectedModel.instituteCode,
            instituteName: selectedModel.instituteName,
            instituteLogo: selectedModel.instituteLogo,
            userID: selectedModel.userID,
            userName: selectedModel.adminName,
            email: selectedModel.adminEmailID,
            instituteAddress: selectedModel.instituteAddress ? selectedModel.instituteAddress.address : '',
            instituteCity: selectedModel.instituteAddress ? selectedModel.instituteAddress.city : '',
            instituteDistrict: selectedModel.instituteAddress ? selectedModel.instituteAddress.district : '',
            instituteState: selectedModel.instituteAddress ? selectedModel.instituteAddress.state : '',
            institutePinCode: selectedModel.instituteAddress ? selectedModel.instituteAddress.pincode : '',
        };
    }

    private getDismissReason(reason: any): string {
        this.message = '';
        this.model = {};
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdp';
        } else {
            return  `with: ${reason}`;
        }
    }

    updateInstitute(content) {
        this.instituteService.updateInstitute(this.model, this.url)
        .subscribe(
            data => {
                if (!data.error) {
                    this.getAllInstitute();
                    this.modalReference.dismiss('Manually');
                } else {
                    this.message = data.error;
                    this.loading = false;
                }
            },
            error => {
                // this.alertService.error('Something went wrong, Please try again later');
                const errorData = this.auth.handleResponse(error);
                if (errorData) {
                    this.message = 'Something went wrong, Please try again later' ;
                    this.loading = false;
                }
            });
    }

    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            const reader = new FileReader();
            // tslint:disable-next-line:no-shadowed-variable
            reader.onload = (event: any) => {
            this.url = event.target.result;
        };
          reader.readAsDataURL(event.target.files[0]);
        }
    }

}
