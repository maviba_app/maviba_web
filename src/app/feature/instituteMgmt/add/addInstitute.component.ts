import { Component, OnInit, Input} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CONFIG } from './../../../app.constant';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { InstituteService, AlertService } from '../../../helpers/services/service';


@Component({
    moduleId: module.id,
    selector: 'app-add-institute',
    templateUrl: './addInstitute.component.html',
    styleUrls: ['./addInstitute.component.scss'],
    providers: [AlertService],
})
export class AddInstituteComponent implements OnInit {
    @Input() mode: string;
    url: string;
    model: any = {};
    loading = false;
    modalReference: any;
    closeResult: string;
    message: string;

    ngOnInit() {
    }

    constructor(
        private router: Router,
        private modalService: NgbModal,
        private instituteService: InstituteService,
        private alertService: AlertService,
    ) { }

    open(content) {
        this.modalReference = this.modalService.open(content, {size : 'lg'});
        this.modalReference.result.then((result) => {
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdp';
        } else {
            return  `with: ${reason}`;
        }
    }

    addInstitute(content) {
        this.instituteService.createInstitute(this.model, this.url)
        .subscribe(
            data => {
                if (!data.error) {
                    window.location.reload();
                    this.modalReference.dismiss('Manually');
                } else {
                    this.message = data.error;
                    this.loading = false;
                }
            },
            error => {
                // this.alertService.error('Something went wrong, Please try again later');
                this.message = 'Something went wrong, Please try again later' ;
                this.loading = false;
            });
    }

    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            const reader = new FileReader();
            // tslint:disable-next-line:no-shadowed-variable
            reader.onload = (event: any) => {
            this.url = event.target.result;
        };
          reader.readAsDataURL(event.target.files[0]);
        }
    }
}
