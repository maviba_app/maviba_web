
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule, BaseRequestOptions } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ThemeModule } from '../../@theme/theme.module';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { CONFIG } from '../../app.constant';

import { AddInstituteComponent } from './add/addInstitute.component';
import { InstituteListComponent } from './institute-list/institute-list.component';
import { InstituteFilterPipe } from '../../helpers/filters/institute-filter.pipe';
import { StarComponent } from '../star/star.component';
import { InstituteService } from '../../helpers/services/institute.service';
import { AlertService, AuthenticationService } from './../../helpers/services/service';
import * as _ from 'underscore';
import { PagerService } from '../../helpers/services/pagination.service';
import { DatePipe } from '@angular/common';



@NgModule({
  imports: [
    CommonModule,
    ThemeModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
    Ng2SmartTableModule,
  ],
  declarations: [AddInstituteComponent, InstituteListComponent, InstituteFilterPipe, StarComponent],
  providers: [InstituteService, AlertService, PagerService],
  exports: [],
})
export class InstituteMgmtModule { }
