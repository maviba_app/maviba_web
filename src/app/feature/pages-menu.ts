import { NbMenuItem } from '@nebular/theme';
import {CONFIG} from '../app.constant';

export const MENU_ITEMS = {
  'superAdmin': [
    {
      title: 'Dashboard',
      icon: 'nb-home',
      link: CONFIG.ROUTES.DASHBOARD,
      home: true,
    },
    {
      title: 'Institute Management',
      icon: 'nb-compose',
      link: CONFIG.ROUTES.INSTITUTE,
    },
    {
      title: 'Academics Setup',
      icon: 'nb-compose',
      link: CONFIG.ROUTES.ACADEMIC_SETUP,
    },
  ],
  'instituteAdmin': [
    {
      title: 'Dashboard',
      icon: 'nb-home',
      link: CONFIG.ROUTES.DASHBOARD,
      home: true,
    },
    {
      title: 'Institute profile',
      icon: 'nb-bar-chart',
      link: CONFIG.ROUTES.INSTITUTE_PROFILE,
    },
    {
      title: 'School Management',
      icon: 'nb-compose',
      link: CONFIG.ROUTES.SCHOOL_MGMT,
    },
    {
      title: 'Academics Setup',
      icon: 'nb-compose',
      link: CONFIG.ROUTES.ACADEMIC_SETUP,
    },
  ],
  'Student': [
    {
      title: 'Dashboard',
      icon: 'nb-home',
      link: CONFIG.ROUTES.DASHBOARD,
      home: true,
    },
  ],
  'Teacher': [
    {
      title: 'Dashboard',
      icon: 'nb-home',
      link: CONFIG.ROUTES.DASHBOARD,
      home: true,
    },
  ],
  'default': [
    {
      title: 'Dashboard',
      icon: 'nb-home',
      link: CONFIG.ROUTES.DASHBOARD,
      home: true,
    },
  ],
};
