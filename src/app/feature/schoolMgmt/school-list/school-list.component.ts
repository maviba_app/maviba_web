import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { PagerService } from '../../../helpers/services/pagination.service';
import { SchoolMgmtService, AlertService } from '../../../helpers/services/service';
import { CONFIG } from '../../../app.constant';
import * as _ from 'underscore';

@Component({
  selector: 'app-school-list',
  templateUrl: './school-list.component.html',
  styleUrls: ['./school-list.component.css'],
  // styles: ['.custom {color : blue}'],
})
export class SchoolListComponent implements OnInit {
  _router: Router;
  listFilter= '';
  schools: any;
  schoolDetail: any;
  selectedSchoolDetail: any;
  // pager object
  pager: any = {};
  // paged items
  pagedItems: any[];
  modalReference: any;
  closeResult: string;
  url: string;
  model: any = {
  };
  loading = false;
  message: string;

    constructor(
        private router: Router,
        private modalService: NgbModal,
        private schoolMgmtService: SchoolMgmtService,
        private alertService: AlertService,
        private pagerService: PagerService,
    ) { }

    ngOnInit() {
        this.schoolMgmtService.getSchool()
        .subscribe(
            schools => {
                if (schools) {
                    this.schools = schools.sort(function(a, b) {
                        return (a.createdDate < b.createdDate) ? 1 : ((b.createdDate < a.createdDate) ? -1 : 0);
                    });
                    this.setPage(1);
                } else {
                }
            },
        );
    }

    openModelWithDetail(content, schoolDetail) {
        this.selectedSchoolDetail = schoolDetail;
        this.modalService.open(content, {size : 'lg'});
    }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        // get pager object from service
        this.pager = this.pagerService.getPager(this.schools.length, page);
        // get current page of items
        this.pagedItems = this.schools.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    open(content, selectedModel) {
        this.model = this.getModelObject(selectedModel);
        this.modalReference = this.modalService.open(content, {size : 'lg'});
        this.modalReference.result.then((result) => {
            this.message = '';
            this.model = {};
          this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.message = '';
            this.model = {};
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        });
    }

    getModelObject(selectedModel) {
        return {
            id : selectedModel.id,
            schoolID: selectedModel.schoolID,
            schoolCode : selectedModel.schoolCode,
            schoolName: selectedModel.schoolName,
            schoolLogo: selectedModel.schoolLogo,
            userID: selectedModel.userID,
            userName: selectedModel.adminName,
            email: selectedModel.adminEmailID,
            schoolAddress: selectedModel.schoolAddress ? selectedModel.schoolAddress.address : '',
            schoolCity: selectedModel.schoolAddress ? selectedModel.schoolAddress.city : '',
            schoolDistrict: selectedModel.schoolAddress ? selectedModel.schoolAddress.district : '',
            schoolState: selectedModel.schoolAddress ? selectedModel.schoolAddress.state : '',
            schoolPinCode: selectedModel.schoolAddress ? selectedModel.schoolAddress.pincode : '',
        };
    }

    private getDismissReason(reason: any): string {
        this.message = '';
        this.model = {};
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdp';
        } else {
            return  `with: ${reason}`;
        }
    }

    updateInstitute(content) {
        this.schoolMgmtService.updateSchool(this.model, this.url)
        .subscribe(
            data => {
                if (!data.error) {
                    window.location.reload();
                    this.modalReference.dismiss('Manually');
                } else {
                    this.message = data.error;
                    this.loading = false;
                }
            },
            error => {
                // this.alertService.error('Something went wrong, Please try again later');
                this.message = 'Something went wrong, Please try again later' ;
                this.loading = false;
            });
    }

    readUrl(event: any) {
        if (event.target.files && event.target.files[0]) {
            const reader = new FileReader();
            // tslint:disable-next-line:no-shadowed-variable
            reader.onload = (event: any) => {
            this.url = event.target.result;
        };
          reader.readAsDataURL(event.target.files[0]);
        }
    }

}
