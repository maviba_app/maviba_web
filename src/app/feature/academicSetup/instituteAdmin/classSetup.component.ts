import { Component, OnInit } from '@angular/core';
import { AcademicSetupService } from '../../../helpers/services/service';
import { CONFIG } from '../../../app.constant';
import * as _ from 'underscore';
import { filter } from 'rxjs/operator/filter';

@Component({
  selector: newFunction(),
  templateUrl: './classSetup.component.html',
})
export class InstituteAdminClassSetupComponent implements OnInit {

  classes: any;
  loading = false;
  message: string;
  titleText: string;
  selection: any;

    constructor(
        private academicSetup: AcademicSetupService,
    ) { }

    ngOnInit() {
        this.academicSetup.getAllClasses()
        .subscribe(
            classes => {
                if (classes) {
                    this.classes = classes;
                } else {
                }
            },
            error => {
            }
        );
    }

    selectedOptions() { // right now: ['1','3']
        return this.classes
              .filter(opt => opt.checked)
              .map(opt => opt)
    }

    saveClass() {

        console.log(this.classes);
        console.log(this.selectedOptions());
    }

}

function newFunction(): string {
    return '<app-institute-admin-class-setup>';
}

