import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { AcademicSetupService } from '../../../helpers/services/service';
import { CONFIG } from '../../../app.constant';
import * as _ from 'underscore';
import { filter } from 'rxjs/operator/filter';

@Component({
  selector: newFunction(),
  templateUrl: './subjectSetup.component.html',
})
export class InstituteAdminSubjectSetupComponent  implements OnInit {
    subjects: any;
    loading = false;
    message: string;
    titleText: string;
    selection: any;

    constructor(
          private academicSetup: AcademicSetupService,
    ) { }

    ngOnInit() {
        this.academicSetup.getAllSubject()
        .subscribe(
            subjects => {
                if (subjects) {
                    this.subjects = subjects;
                } else {
                }
            },
            error => {
            }
        );
    }

    selectedOptions() { // right now: ['1','3']
        return this.subjects
            .filter(opt => opt.checked)
            .map(opt => opt)
    }

    saveSubject() {

        console.log(this.subjects);
        console.log(this.selectedOptions());
    }

}

function newFunction(): string {
    return '<app-institute-admin-subject-setup>';
}

