import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  template: `
    <ngx-sample-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-sample-layout>
  `,
})
export class PagesComponent {

    currentUser = localStorage.getItem('loggedInUser');
    user = JSON.parse(this.currentUser);
    role = this.user && this.user.userDetails ? this.user.userDetails.userRole : '';
    menus = this.role ? MENU_ITEMS[this.role] : MENU_ITEMS['default'];
    menu = this.menus ? this.menus : MENU_ITEMS['default'];
}
