import { NgModule } from '@angular/core';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardComponent } from './dashboard.component';
import { RouterModule, Routes } from '@angular/router';

@NgModule({
  imports: [
    ThemeModule,
  ],
  declarations: [
    DashboardComponent
  ],
})
export class DashboardModule { }
