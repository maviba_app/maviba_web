import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnalyticsService } from './@theme/utils/analytics.service';
import { CONFIG } from './app.constant';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = CONFIG.MSG.appTitle;
  constructor(private analytics: AnalyticsService) {
  }

  ngOnInit(): void {
    this.analytics.trackPageViews();
  }
}
