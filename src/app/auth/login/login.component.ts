import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CONFIG } from '../../app.constant';
import { AlertService, AuthenticationService, InstituteService } from './../../helpers/services/service';

@Component({
    moduleId: module.id,
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    providers: [AuthenticationService, AlertService ],
})
export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService,
        private instituteService: InstituteService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
        .subscribe(
            data => {
                if (!data.error) {
                    localStorage.setItem('loggedInUser', JSON.stringify(data));
                    this.router.navigate([CONFIG.ROUTES.PRIVATE]);
                } else {
                    this.alertService.error('Invalid username and password');
                    this.loading = false;
                }
            },
            error => {
                this.alertService.error('Something went wrong, Please try again later');
                this.loading = false;
            });
    }
}
