export const CONFIG = {
    'URL': {
        'BASE': 'http://139.59.40.232:3000/api/',
        'LOGIN': 'users/login',
        'GET_ALL_INSTITUTE': 'institute/getInstituteCount',
        'CREATE_INSTITUTE': 'institute/createInstitute',
        'UPDATE_INSTITUTE': 'institute/updateInstitute',
        'GET_INSTITUTE_DETAIL': 'institute/getInstituteDetails',
        'GET_ALL_SCHOOL': 'school/getSchoolsList',
        'CREATE_SCHOOL': 'school/createSchool',
        'GET_SCHOOL_DETAIL': 'school/getSchoolDetails',
        'UPDATE_SCHOOL': 'school/updateSchool',
        'GET_ALL_CLASSES': 'setUp/classes',
    },
    'MSG': {
        'appTitle': 'Institute management system',
    },
    'ROUTES': {
        'LOGIN': 'login',
        'PRIVATE': 'private',
        'DASHBOARD': 'dashboard',
        'INSTITUTE': 'institutes',
        'INSTITUTE_PROFILE': 'instituteProfile',
        'SCHOOL_MGMT': 'schools',
        'WELCOME': 'welcome',
        'ACADEMIC_SETUP': 'acedemicSetUp',
    },
    'ROLES': {
        'SUPER_ADMIN': 'superAdmin',
        'INSTITUTE_ADMIN': 'instituteAdmin',
    },
};
